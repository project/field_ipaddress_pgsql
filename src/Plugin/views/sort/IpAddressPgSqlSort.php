<?php

namespace Drupal\field_ipaddress_pgsql\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Sort handler for the "IP Address PostgreSQL inet" field type.
 *
 * @ViewsSort("ipaddress_pgsql")
 */
class IpAddressPgSqlSort extends SortPluginBase {

  /**
   * Called to add the sort to a query.
   */
  public function query() {
    $this->ensureMyTable();
    $this->query->addOrderBy(NULL, "inet($this->tableAlias.$this->realField)", $this->options['order'], $this->realField);
  }

}
