<?php

/**
 * @file
 * Views hooks for the Field IP address PostgreSQL module.
 */

declare(strict_types=1);

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 *
 * Add a custom views exposed filter and a sort plugin.
 */
function field_ipaddress_pgsql_field_views_data_alter(array &$data, FieldStorageConfigInterface $field) {
  if ($field->getType() === 'ipaddress_pgsql') {
    $field_name = $field->getName() . '_' . $field->getMainPropertyName();

    foreach ($data as $table_name => $table_data) {
      $help = t('A filter that uses the <a href=":url" target="_blank">IP Address Operators</a> provided by the PostgreSQL.', [':url' => 'https://www.postgresql.org/docs/16/functions-net.html']);

      $data[$table_name][$field_name]['filter'] = [
        'title' => t('IP Address PostgreSQL'),
        'help' => $help,
        'id' => 'ipaddress_pgsql',
      ];

      $data[$table_name][$field_name]['sort'] = [
        'id' => 'ipaddress_pgsql',
      ];
    }
  }
}
