<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field_ipaddress_pgsql\Plugin\Field\FieldType\IpAddressMySqlField;
use IPTools\Network;

/**
 * Plugin implementation of the 'IP Address MySQL' formatter.
 *
 * @FieldFormatter(
 *   id = "ipaddress_mysql",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "ipaddress_mysql"
 *   }
 * )
 */
final class IpAddressMySqlFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'function' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $options = [
      'host' => 'host',
      'masklen' => 'masklen',
      'netmask' => 'netmask',
      'network' => 'network',
      'text' => 'text',
    ];

    if ($this->getFieldSetting('default_gateway_enabled')) {
      $options['gateway'] = 'gateway';
      $options['address_and_prefix'] = 'address and prefix';
    }

    $form['function'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->getSetting('function'),
      '#empty_value' => '',
      '#sort_options' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('IP Address function to display additional information about the IP address.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = $this->viewValue($item);
    }

    return $element;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\field_ipaddress_pgsql\Plugin\Field\FieldType\IpAddressMySqlField $field
   *   The field item of the "ipaddress_mysql" field type.
   *
   * @return array
   *   The textual output generated as a render array.
   */
  protected function viewValue(IpAddressMySqlField $field) {
    $ipAddress = inet_ntop($field->ip_address);
    $networkMask = inet_ntop($field->network_mask);
    // If a user has not selected any functions in the formatter settings, then
    // return the IP address in the CIDR notation, like "192.168.100.1/24".
    if ($this->getSetting('function') === '') {
      $prefixLength = Network::parse("$ipAddress $networkMask")->getPrefixLength();
      $value = "$ipAddress/$prefixLength";
      // If the Default gateway option was selected then also render a gateway.
      if ($this->getFieldSetting('default_gateway_enabled')) {
        return [
          '#theme' => 'field__ipaddress_default_gateway',
          '#value' => $value,
          '#default_gateway' => inet_ntop($field->default_gateway),
        ];
      }
      else {
        return [
          '#theme' => 'field__ipaddress',
          '#value' => $value,
        ];
      }
    }

    $function = $this->getSetting('function');
    $value = '';

    switch ($function) {
      case 'address_and_prefix':
        $prefixLength = Network::parse("$ipAddress $networkMask")->getPrefixLength();
        $value = "$ipAddress/$prefixLength";
        break;

      case 'gateway':
        $value = inet_ntop($field->default_gateway);
        break;

      case 'host':
        $value = Network::parse("$ipAddress $networkMask")->getIP();
        break;

      case 'masklen':
        $cidr = Network::parse("$ipAddress $networkMask")->getCIDR();
        $parts = explode('/', $cidr);
        $value = $parts[1];
        break;

      case 'netmask':
        $value = $networkMask;
        break;

      case 'network':
        $value = Network::parse("$ipAddress $networkMask")->getNetwork();
        break;

      case 'text':
        $value = $ipAddress . ' ' . $networkMask;
    }

    return [
      '#theme' => 'field__ipaddress',
      '#value' => $value,
    ];

  }

}
