<?php

namespace Drupal\Tests\field_ipaddress_pgsql\Functional;

use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the IP address field functionality.
 *
 * @group field_ipaddress_pgsql
 */
class IpAddressFieldTest extends IpAddressFieldTestBase {

  /**
   * Tests the IP Address field with different settings and values.
   *
   * @param array $fieldStorageSettings
   *   The ip field instance settings.
   * @param string $value
   *   The IP address value.
   * @param string $expected
   *   The expected output for the field.
   * @param string $error
   *   The expected error message.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ElementTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @dataProvider ipAddressFieldDataProvider
   */
  public function testIpAddressField(array $fieldStorageSettings, string $value, string $expected = '', string $error = '') {
    // Apply field settings.
    $fieldStorage = FieldStorageConfig::loadByName('node', $this->fieldName);
    $fieldStorage->set('settings', $fieldStorageSettings)->save();

    // Create new node of test type.
    $this->drupalGet('node/add/' . $this->nodeType->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm([
      'title[0][value]' => 'Test node',
      $this->fieldName . '[0][value]' => $value,
    ], 'Save');

    if ($error) {
      $this->assertSession()->statusMessageContains($error, 'error');
    }
    elseif ($expected) {
      $this->assertSession()->addressMatches('/^\/node\/\d$/');
      $this->assertSession()->elementTextContains('css', 'article', $expected);
    }
  }

  /**
   * Data provider for testIpAddressField.
   */
  public function ipAddressFieldDataProvider() {
    return [
      // ---------- Data for "Positive Testing" -----------.
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => FALSE,
        ],
        '192.168.100.10/24',
        '192.168.100.10/24',
      ],
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => FALSE,
        ],
        '192.168.100.10',
        '192.168.100.10/32',
      ],
      [
        [
          'family' => 4,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => FALSE,
        ],
        '192.168.100.0/24',
        '192.168.100.0/24',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => FALSE,
        ],
        '2001:4f8:3:ba::/64',
        '2001:4f8:3:ba::/64',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => FALSE,
        ],
        '::ffff:1.2.3.0/120',
        '::ffff:1.2.3.0/120',
      ],
      // ---------- Data for "Negative Testing" -----------.
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => FALSE,
        ],
        'qwerty',
        '',
        'Invalid IP address format',
      ],
      // Invalid mask length.
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => FALSE,
        ],
        '192.168.100.10/33',
        '',
        'Invalid IP address format',
      ],
      [
        [
          'family' => 4,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => FALSE,
        ],
        '192.168.100.10/24',
        '',
        'The IP address value must follow',
      ],
      // The "::" sign can only appear once in a single IP address.
      [
        [
          'family' => 6,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => FALSE,
        ],
        '2001:db8:a0b:12f0::::0:1',
        '',
        'Invalid IP address format',
      ],
      // Invalid mask length.
      [
        [
          'family' => 6,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => FALSE,
        ],
        '2001:4f8:3:ba::/129',
        '',
        'Invalid IP address format',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => FALSE,
        ],
        '2001:db8:1234::1/48',
        '',
        'The IP address value must follow',
      ],
    ];
  }

}
