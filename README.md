## INTRODUCTION

The module provides a field type that is stored and processed as the PostgreSQL
[Network Address Types](https://www.postgresql.org/docs/current/datatype-net-types.html#DATATYPE-NET-TYPES).
The "IP address" field type, formatter and views exposed filter are available.
The exposed filter uses the [IP Address Operators](https://www.postgresql.org/docs/16/functions-net.html)
provided by the PostgreSQL.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Add to a content type a field of the "IP address" field type.
- Visit the "Manage display" page of the content type and configure the "Format
settings" of the field.

## MAINTAINERS

- Andrey Vitushkin (wombatbuddy) - https://www.drupal.org/u/wombatbuddy
