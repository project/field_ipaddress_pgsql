<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use IPTools\Network;

/**
 * Plugin implementation of the 'IP Address PostgreSQL' widget.
 *
 * @FieldWidget(
 *   id = "ipaddress_mysql",
 *   label = @Translation("IP Address MySQL"),
 *   field_types = {
 *     "ipaddress_mysql"
 *   }
 * )
 */
final class IpAddressMySqlWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    if ($items[$delta]->ip_address && $items[$delta]->network_mask) {
      $ipAddress = inet_ntop($items[$delta]->ip_address);
      $networkMask = inet_ntop($items[$delta]->network_mask);
      $prefixLength = Network::parse("$ipAddress $networkMask")->getPrefixLength();
      $defaultValue = "$ipAddress/$prefixLength";
    }
    else {
      $defaultValue = NULL;
    }

    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $defaultValue,
    ];

    if ($this->getFieldSetting('default_gateway_enabled')) {
      // We need this check to prevent the error that occurs when a user open
      // the field settings on the "Manage fields" page.
      if ($items[$delta]->default_gateway) {
        $defaultGateway = inet_ntop($items[$delta]->default_gateway);
      }
      else {
        $defaultGateway = '';
      }

      $element['default_gateway'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Default Gateway'),
        '#default_value' => $defaultGateway,
      ];
    }

    return $element;
  }

  /**
   * Separate the IP address and network mask and convert them to binary string.
   *
   * We convert them to binary string so that they can be stored in a column of
   * the VARBINARY(16) type.
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      $value = trim($item['value']);
      // We need the "try" block here because the validate() method of the
      // MySqlFormatConstraintValidator class runs after this method.
      try {
        $ipAddress = (string) Network::parse($value)->getIP();
        $networkMask = (string) Network::parse($value)->getNetmask();
        $item['ip_address'] = inet_pton($ipAddress);
        $item['network_mask'] = inet_pton($networkMask);
        if ($this->getFieldSetting('default_gateway_enabled')) {
          $item['default_gateway'] = inet_pton(trim($item['default_gateway']));
        }
      }
      catch (\Exception $e) {
        // Set some values, because if the values are empty, then the validate()
        // method of the MySqlFormatConstraintValidator class is not called and
        // the validation does not occur. We do this to avoid adding an element
        // validator handler in the formElement() methods.
        $item['ip_address'] = 'invalid';
        $item['network_mask'] = 'invalid';
        if ($this->getFieldSetting('default_gateway_enabled')) {
          $item['default_gateway'] = 'invalid';
        }
      }
    }
    return $values;
  }

}
