<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Field\FieldWidget;

use Drupal\Core\Database\Connection;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'IP Address PostgreSQL' widget.
 *
 * @FieldWidget(
 *   id = "ipaddress_pgsql",
 *   label = @Translation("IP Address PostgreSQL"),
 *   field_types = {
 *     "ipaddress_pgsql"
 *   }
 * )
 */
final class IpAddressPostgreSqlWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    private readonly Connection $connection,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    // Check if the IP address was entered with netmask prefix and if not then
    // get the representation with prefix by calling the 'text' function
    // provided by PostgreSQL.
    $ipAddress = $items[$delta]->value;

    if ($ipAddress) {
      $parts = explode('/', $ipAddress);
      // If the IP address was stored without netmask prefix.
      if (count($parts) == 1) {
        // Get the IP address with the prefix format like this:
        $query = $this->connection->query("SELECT text(inet :ipAddressString)", [
          ':ipAddressString' => $ipAddress,
        ]);
        $ipAddress = $query->fetchField();
      }
    }

    $element['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP address'),
      '#default_value' => $ipAddress,
    ];

    if ($this->getFieldSetting('default_gateway_enabled')) {
      $element['default_gateway'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Default Gateway'),
        '#default_value' => $items[$delta]->default_gateway ?? '',
      ];
    }

    return $element;
  }

  /**
   * Remove spaces at the beginning and at the end of the entered value.
   *
   * We allow a user to enter spaces at the beginning and end of the entered
   * value. That's why we try to remove them.
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      $item['value'] = trim($item['value']);

      if ($this->getFieldSetting('default_gateway_enabled')) {
        $item['default_gateway'] = trim($item['default_gateway']);
      }
    }

    return $values;
  }

}
