<?php

namespace Drupal\Tests\field_ipaddress_pgsql\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the ip field functionality.
 *
 * @group field_ipaddress_pgsql
 */
class IpAddressFieldTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ipaddress_pgsql',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The node type to use in tests.
   *
   * @var \Drupal\node\NodeTypeInterface
   */
  protected $nodeType;

  /**
   * The field name to use in tests.
   *
   * @var string
   */
  protected $fieldName = 'field_test_ip';

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->nodeType = $this->drupalCreateContentType(['type' => 'test_type']);

    $this->drupalLogin($this->createUser([
      'create test_type content',
    ]));

    // Under the hood, the module provides separate field types for PostgreSQL
    // and MySQL database. Accordingly, the tests will be performed in two
    // different environments. We need to check which database type is used to
    // test the corresponding field type.
    if (\Drupal::database()->driver() === 'pgsql') {
      $fieldType = 'ipaddress_pgsql';
    }
    else {
      $fieldType = 'ipaddress_mysql';
    }

    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => $fieldType,
      'settings' => [],
    ]);
    $fieldStorage->save();

    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'settings' => [],
    ]);
    $field->save();

    // Configure the displays to make sure field is shown on node form and view.
    $form = \Drupal::configFactory()
      ->getEditable('core.entity_form_display.node.' . $this->nodeType->id() . '.default');
    $form->set('content.' . $this->fieldName . '.type', 'ipaddress_default')
      ->set('content.' . $this->fieldName . '.settings', [])
      ->set('content.' . $this->fieldName . '.third_party_settings', [])
      ->set('content.' . $this->fieldName . '.weight', 0)
      ->save();
    // Configure the widget to make sure field in shown on node form.
    $display = \Drupal::configFactory()
      ->getEditable('core.entity_view_display.node.' . $this->nodeType->id() . '.default');
    $display->set('content.' . $this->fieldName . '.type', 'ipaddress_default')
      ->set('content.' . $this->fieldName . '.label', 'hidden')
      ->set('content.' . $this->fieldName . '.settings', [])
      ->set('content.' . $this->fieldName . '.third_party_settings', [])
      ->set('content.' . $this->fieldName . '.weight', 0)
      ->save();
  }

}
