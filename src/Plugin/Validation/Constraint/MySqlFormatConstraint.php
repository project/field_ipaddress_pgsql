<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a inet format validation constraint.
 *
 * @Constraint(
 *   id = "MySqlFormat",
 *   label = @Translation("MySQL format validation", context = "Validation"),
 * )
 */
final class MySqlFormatConstraint extends Constraint {

  /**
   * Message when the IP address format is invalid for both IPv4 and IPv6.
   *
   * @var string
   */
  public string $messageIpAddressFormat = 'Invalid IP address format.';

  /**
   * Message when the value isn't in the IPv4 format.
   *
   * @var string
   */
  public string $messageIpV4 = 'The IP address is not in the IPv4 format.';

  /**
   * Message when the value isn't in the IPv6 format.
   *
   * @var string
   */
  public string $messageIpV6 = 'The IP address is not in the IPv6 format.';

  /**
   * Message when the value isn't in the CIDR format.
   *
   * @var string
   */
  public string $messageCidr = 'The IP address value must follow <a href="https://www.postgresql.org/docs/16/datatype-net-types.html#DATATYPE-CIDR" target="_blank">CIDR notation</a>.';

  /**
   * Message when the default gateway is not walid.
   *
   * @var string
   */
  public string $defaultGateway = 'Invalid default gateway. The value should be between @firstIP and @lastIP.';

}
