<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use IPTools\Network;

/**
 * Plugin implementation of the 'IP Address PostgreSQL' field type.
 *
 * @FieldType(
 *   id = "ipaddress_pgsql",
 *   label = @Translation("IP address"),
 *   description = @Translation("Stores an IP address."),
 *   weight = -100,
 *   default_widget = "ipaddress_pgsql",
 *   default_formatter = "ipaddress_pgsql",
 *   constraints = {"PostgreSqlFormat" = {}}
 * )
 */
final class IpAddressPostgreSqlField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'family' => '4',
      'cidr_format' => FALSE,
      'default_gateway_enabled' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];

    $element['family'] = [
      '#type' => 'radios',
      '#title' => $this->t('IP version allowed'),
      '#options' => [
        '4' => $this->t('IPv4'),
        '6' => $this->t('IPv6'),
      ],
      '#description' => $this->t('Select the IP address family that are allowed.'),
      '#default_value' => $this->getSetting('family'),
      '#disabled' => $has_data,
    ];

    $description = $this->t('If checked, only allow addresses that comply with <a href=":url" target="_blank">CIDR</a> notation.', [
      ':url' => 'https://www.postgresql.org/docs/16/datatype-net-types.html#DATATYPE-CIDR',
    ]);

    $element['cidr_format'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('CIDR'),
      '#description' => $description,
      '#default_value' => $this->getSetting('cidr_format'),
      '#disabled' => $has_data,
    ];

    $element['default_gateway_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add default gateway'),
      '#default_value' => $this->getSetting('default_gateway_enabled'),
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return match ($this->get('value')->getValue()) {
      NULL, '' => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];
    $properties['value'] = DataDefinition::create('any')
      ->setLabel(t('Value'));

    // If the 'default_gateway_enabled' option was checked then add a property
    // for the default gateway.
    if ($field_definition->getSetting('default_gateway_enabled')) {
      $properties['default_gateway'] = DataDefinition::create('any')
        ->setLabel(t('Default gateway'));
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    // We add the "type" key just to prevent error messages like "Warning:
    // Undefined array key "type" in views_field_default_views_data()..."
    $columns = [
      'value' => [
        'pgsql_type' => 'inet',
        'not null' => TRUE,
        'description' => 'The IP address.',
        'type' => '',
      ],
    ];

    // Adding the GIST index failed, the hack that is suggested here
    // https://www.drupal.org/node/3264989 didn't work.
    // Also, there is an open issue:
    // https://www.drupal.org/project/drupal/issues/3397622
    $indexes = [
      'value' => [
        'value',
      ],
    ];

    // If the 'default_gateway_enabled' option was checked then add a column to
    // store the default gateway.
    if ($field_definition->getSetting('default_gateway_enabled')) {
      $columns['default_gateway'] = [
        'pgsql_type' => 'inet',
        'not null' => TRUE,
        'description' => 'Default gateway.',
        'type' => '',
      ];

      $indexes['default_gateway'] = ['default_gateway'];
    }

    $schema = [
      'columns' => $columns,
      'indexes' => $indexes,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $settings = $field_definition->getSettings();
    // IPv4.
    if ($settings['family'] == 4) {
      // Number of bytes in IP address.
      $bites = 4;
      $prefixMaxLength = 32;
    }
    else {
      // IPv6.
      // Number of bytes in IP address.
      $bites = 16;
      $prefixMaxLength = 128;
    }

    $ipAddres = inet_ntop(random_bytes($bites));
    $prefixLength = mt_rand(1, $prefixMaxLength);
    $value = "$ipAddres/$prefixLength";

    if ($settings['cidr_format']) {
      $value = (string) Network::parse($value)->CIDR;
    }

    $values['value'] = $value;

    if ($settings['default_gateway_enabled']) {
      /** @var \IPTools\Range $hosts */
      $hosts = Network::parse($value)->hosts;
      // For simplicity, we will randomly select either the first or last value
      // from the range of valid values.
      if (mt_rand(0, 1) == 0) {
        $defaultGateway = (string) $hosts->getFirstIP();
      }
      else {
        $defaultGateway = (string) $hosts->getLastIP();
      }
      $values['default_gateway'] = $defaultGateway;
    }

    return $values;
  }

}
