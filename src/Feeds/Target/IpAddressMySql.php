<?php

namespace Drupal\field_ipaddress_pgsql\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;
use IPTools\Network;

/**
 * Defines a field mapper for "ipaddress_mysql" Field.
 *
 * @FeedsTarget(
 *   id = "ipaddress_mysql",
 *   field_types = {"ipaddress_mysql"}
 * )
 */
class IpAddressMySql extends FieldTargetBase {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    $definition = FieldTargetDefinition::createFromFieldDefinition($field_definition);
    $definition->addProperty('value');

    if ($field_definition->getSetting('default_gateway_enabled')) {
      $definition->addProperty('default_gateway');
    }
    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    $value = $values['value'];
    $ipAddress = (string) Network::parse($value)->getIP();
    $networkMask = (string) Network::parse($value)->getNetmask();
    $values['ip_address'] = inet_pton($ipAddress);
    $values['network_mask'] = inet_pton($networkMask);

    if ($this->settings['default_gateway_enabled']) {
      $values['default_gateway'] = inet_pton($values['default_gateway']);
    }

  }

}
