<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use IPTools\Network;

/**
 * Plugin implementation of the 'IP Address MySQL' field type.
 *
 * @FieldType(
 *   id = "ipaddress_mysql",
 *   label = @Translation("IP address"),
 *   description = @Translation("Stores an IP address."),
 *   weight = -100,
 *   default_widget = "ipaddress_mysql",
 *   default_formatter = "ipaddress_mysql",
 *   constraints = {"MySqlFormat" = {}}
 * )
 */
final class IpAddressMySqlField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'family' => '4',
      'cidr_format' => FALSE,
      'default_gateway_enabled' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];

    $element['family'] = [
      '#type' => 'radios',
      '#title' => $this->t('IP version allowed'),
      '#options' => [
        '4' => $this->t('IPv4'),
        '6' => $this->t('IPv6'),
      ],
      '#description' => $this->t('Select the IP address family that are allowed.'),
      '#default_value' => $this->getSetting('family'),
      '#disabled' => $has_data,
    ];

    $description = $this->t('If checked, only allow addresses that comply with <a href=":url" target="_blank">CIDR</a> notation.', [
      ':url' => 'https://www.postgresql.org/docs/16/datatype-net-types.html#DATATYPE-CIDR',
    ]);

    $element['cidr_format'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('CIDR'),
      '#description' => $description,
      '#default_value' => $this->getSetting('cidr_format'),
      '#disabled' => $has_data,
    ];

    $element['default_gateway_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add default gateway'),
      '#default_value' => $this->getSetting('default_gateway_enabled'),
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return match ($this->get('ip_address')->getValue()) {
      NULL, '' => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];
    $properties['ip_address'] = DataDefinition::create('any')
      ->setLabel(t('IP address'));

    $properties['network_mask'] = DataDefinition::create('any')
      ->setLabel(t('Network mask'));

    // If the 'default_gateway_enabled' option was checked then add a property
    // for the default gateway.
    if ($field_definition->getSetting('default_gateway_enabled')) {
      $properties['default_gateway'] = DataDefinition::create('any')
        ->setLabel(t('Default gateway'));
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    // We add the "type" key just to prevent error messages like "Warning:
    // Undefined array key "type" in views_field_default_views_data()..."
    $columns = [
      'ip_address' => [
        'mysql_type' => 'VARBINARY(16)',
        'not null' => TRUE,
        'description' => 'The IP address.',
        'type' => '',
      ],
      'network_mask' => [
        'mysql_type' => 'VARBINARY(16)',
        'not null' => TRUE,
        'description' => 'The network mask.',
        'type' => '',
      ],
    ];

    $indexes = [
      'ip_address' => ['ip_address'],
      'network_mask' => ['network_mask'],
    ];

    // If the 'default_gateway_enabled' option was checked then add a column to
    // store the default gateway.
    if ($field_definition->getSetting('default_gateway_enabled')) {
      $columns['default_gateway'] = [
        'mysql_type' => 'VARBINARY(16)',
        'not null' => TRUE,
        'description' => 'Default gateway.',
        'type' => '',
      ];

      $indexes['default_gateway'] = ['default_gateway'];
    }

    $schema = [
      'columns' => $columns,
      'indexes' => $indexes,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Inject the service when the following issue are resolved
   * https://www.drupal.org/node/2053415
   * https://www.drupal.org/project/drupal/issues/3294266
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $settings = $field_definition->getSettings();

    // IPv4.
    if ($settings['family'] == 4) {
      // Number of bytes in IP address.
      $bites = 4;
      $prefixMaxLength = 32;
    }
    else {
      // IPv6.
      // Number of bytes in IP address.
      $bites = 16;
      $prefixMaxLength = 128;
    }
    // Generate an IP Address in the text format.
    $ipAddres = inet_ntop(random_bytes($bites));
    $prefixLength = mt_rand(1, $prefixMaxLength);
    $networkMask = (string) Network::parse("$ipAddres/$prefixLength")->getNetmask();
    $values['network_mask'] = inet_pton($networkMask);

    if ($settings['cidr_format']) {
      $cidr = Network::parse("$ipAddres/$prefixLength")->CIDR;
      $ipAddres = explode('/', $cidr)[0];
    }

    $values['ip_address'] = inet_pton($ipAddres);

    if ($settings['default_gateway_enabled']) {
      /** @var \IPTools\Range $hosts */
      $hosts = Network::parse("$ipAddres/$prefixLength")->hosts;
      // For simplicity, we will randomly select either the first or last value
      // from the range of valid values.
      if (mt_rand(0, 1) == 0) {
        $defaultGateway = (string) $hosts->getFirstIP();
      }
      else {
        $defaultGateway = (string) $hosts->getLastIP();
      }
      $values['default_gateway'] = inet_pton($defaultGateway);
    }

    return $values;
  }

}
