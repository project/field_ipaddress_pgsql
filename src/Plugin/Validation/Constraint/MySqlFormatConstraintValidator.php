<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Validation\Constraint;

use IPTools\Network;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the MySQL format validation constraint.
 */
final class MySqlFormatConstraintValidator extends ConstraintValidator {

  /**
   * Validate the IP address.
   */
  public function validate(mixed $field, Constraint $constraint): void {
    $value = $field->value;

    try {
      /** @var \IPTools\IP $ipAddress */
      $ipAddress = Network::parse($value)->getIP();
    }
    catch (\Exception $e) {
      $this->context->addViolation($constraint->messageIpAddressFormat);
      return;
    }

    /** @var \Drupal\Core\Field\FieldItemListInterface $field */
    $settings = $field->getFieldDefinition()->getSettings();

    if ($settings['family'] == 4) {
      if ($ipAddress->getVersion() !== $ipAddress::IP_V4) {
        $this->context->addViolation($constraint->messageIpV4);
        return;
      }
    }
    elseif ($ipAddress->getVersion() !== $ipAddress::IP_V6) {
      $this->context->addViolation($constraint->messageIpV6);
      return;
    }

    if ($settings['cidr_format']) {
      $network = (string) Network::parse($value)->getNetwork();
      $ipAddress = (string) $ipAddress;
      if ($ipAddress !== $network) {
        $this->context->addViolation($constraint->messageCidr);
      }
    }

    // Validate the Default gateway if it was enabled in the field settings.
    // The "Default Gateway" is an IP Address that is within the subnet of the
    // "Network Interface". If the value of the "Network Interface" is
    // 192.168.100.10/24, then the valid value of "Default Gateway" should be
    // between 192.168.100.1 and 192.168.100.254.
    if (!$settings['default_gateway_enabled']) {
      return;
    }

    /** @var \IPTools\Range $hosts */
    $hosts = Network::parse($value)->hosts;

    // As in the "ipaddress_mysql" widget and in the IpAddressMySql @FeedsTarget
    // plugin we applied to the value the inet_pton() function, then we need to
    // convert it back to the human-readable representation.
    // If a syntactically invalid Default Gateway was given then inet_pton()
    // return false. So we should check this first.
    if ($field->default_gateway) {
      $defaultGateway = inet_ntop($field->default_gateway);
    }
    else {
      $this->context->addViolation($constraint->defaultGateway, [
        '@firstIP' => (string) $hosts->getFirstIP(),
        '@lastIP' => (string) $hosts->getLastIP(),
      ]);
      return;
    }

    try {
      /** @var \IPTools\IP $ipAddress */
      $defaultGateway = Network::parse($defaultGateway)->getIP();
    }
    catch (\Exception $e) {
      $this->context->addViolation($constraint->defaultGateway, [
        '@firstIP' => (string) $hosts->getFirstIP(),
        '@lastIP' => (string) $hosts->getLastIP(),
      ]);
      return;
    }

    if (!$hosts->contains($defaultGateway)) {
      $this->context->addViolation($constraint->defaultGateway, [
        '@firstIP' => (string) $hosts->getFirstIP(),
        '@lastIP' => (string) $hosts->getLastIP(),
      ]);
    }

  }

}
