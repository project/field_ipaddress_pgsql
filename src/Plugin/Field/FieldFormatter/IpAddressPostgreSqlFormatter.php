<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Field\FieldFormatter;

use Drupal\Core\Database\Connection;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\field_ipaddress_pgsql\Plugin\Field\FieldType\IpAddressPostgreSqlField;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'IP Address PostgreSQL' formatter.
 *
 * @FieldFormatter(
 *   id = "ipaddress_pgsql",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "ipaddress_pgsql"
 *   }
 * )
 */
final class IpAddressPostgreSqlFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    private readonly Connection $connection,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'function' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['link_to_functions'] = [
      '#type' => 'link',
      '#title' => $this->t('IP Address Function'),
      '#url' => Url::fromUri('https://www.postgresql.org/docs/16/functions-net.html'),
      '#attributes' => [
        'target' => '_blank',
      ],
      '#prefix' => '<div>',
      '#sufix' => '</div>',
    ];

    $options = [
      'abbrev' => 'abbrev',
      'broadcast' => 'broadcast',
      'family' => 'family',
      'host' => 'host',
      'hostmask' => 'hostmask',
      'masklen' => 'masklen',
      'netmask' => 'netmask',
      'network' => 'network',
      'text' => 'text',
    ];

    if ($this->getFieldSetting('default_gateway_enabled')) {
      $options['gateway'] = 'gateway';
      $options['address_and_prefix'] = 'address and prefix';
    }

    $form['function'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->getSetting('function'),
      '#empty_value' => '',
      '#sort_options' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('IP Address function to display additional information about the IP address.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = $this->viewValue($item);
    }
    return $element;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\field_ipaddress_pgsql\Plugin\Field\FieldType\IpAddressPostgreSqlField $field
   *   The field item of the "ipaddress_pgsql" field type.
   *
   * @return array
   *   The textual output generated as a render array.
   */
  protected function viewValue(IpAddressPostgreSqlField $field) {
    $ipAddressString = $field->value;
    // If a user has not selected any functions in the formatter settings,
    // then return the result of the 'text' function. We use the 'text'
    // because it enables to display the IP address with the netmask prefix even
    // if the IP address was entered without it.
    if ($this->getSetting('function') === '') {
      $select = "SELECT text(inet :ipAddressString)";
      $query = $this->connection->query($select, [':ipAddressString' => $ipAddressString]);
      $value = $query->fetchField();
      // If the Default gateway option was selected then also render a gateway.
      if ($this->getFieldSetting('default_gateway_enabled')) {
        return [
          '#theme' => 'field__ipaddress_default_gateway',
          '#value' => $value,
          '#default_gateway' => $field->default_gateway,
        ];
      }
      else {
        return [
          '#theme' => 'field__ipaddress',
          '#value' => $value,
        ];
      }
    }

    // As a user has selected a function in the formatter settings, then we
    // create a query to the database and get the values of these function.
    $function = $this->getSetting('function');

    $select = '';
    // We don't use the $function in the placeholders to prevent single quotes.
    switch ($function) {
      case 'abbrev':
        $select = "SELECT abbrev(inet :ipAddressString)";
        break;

      case 'address_and_prefix':
        $select = "SELECT text(inet :ipAddressString)";
        break;

      case 'broadcast':
        $select = "SELECT broadcast(inet :ipAddressString)";
        break;

      case 'family':
        $select = "SELECT family(inet :ipAddressString)";
        break;

      case 'host':
        $select = "SELECT host(inet :ipAddressString)";
        break;

      case 'hostmask':
        $select = "SELECT hostmask(inet :ipAddressString)";
        break;

      case 'masklen':
        $select = "SELECT masklen(inet :ipAddressString)";
        break;

      case 'netmask':
        $select = "SELECT netmask(inet :ipAddressString)";
        break;

      case 'network':
        $select = "SELECT network(inet :ipAddressString)";
        break;

      case 'text':
        $select = "SELECT text(inet :ipAddressString)";
    }

    // If was checked the "Add default gateway" option, we are not querying the
    // database but just render the gateway value.
    if ($function === 'gateway') {
      $result = $field->default_gateway;
    }
    else {
      $query = $this->connection->query($select, [':ipAddressString' => $ipAddressString]);
      $result = $query->fetchField();
    }

    return [
      '#theme' => 'field__ipaddress',
      '#value' => $result,
    ];

  }

}
