<?php

namespace Drupal\field_ipaddress_pgsql\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * A filter that uses the "IP Address Operators" provided by the PostgreSQL.
 *
 * @see https://www.postgresql.org/docs/16/functions-net.html#FUNCTIONS-NET
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("ipaddress_pgsql")
 */
class IpAddressPgSqlFilter extends StringFilter {

  /**
   * Get the operators.
   */
  public function operators() {

    $operators = [
      '=' => [
        'title' => $this->t('Is equal to?'),
        'short' => $this->t('='),
        'values' => 1,
      ],
      '<<' => [
        'title' => $this->t('Is subnet strictly contained by subnet?'),
        'short' => $this->t('<<'),
        'values' => 1,
      ],
      '<<=' => [
        'title' => $this->t('Is subnet contained by or equal to subnet?'),
        'short' => $this->t('<<='),
        'values' => 1,
      ],
      '>>' => [
        'title' => $this->t('Does subnet strictly contain subnet?'),
        'short' => $this->t('>>'),
        'values' => 1,
      ],
      '>>=' => [
        'title' => $this->t('Does subnet contain or equal subnet?'),
        'short' => $this->t('>>='),
        'values' => 1,
      ],
      '&&' => [
        'title' => $this->t('Does either subnet contain or equal the other?'),
        'short' => $this->t('&&'),
        'values' => 1,
      ],
    ];

    return $operators;
  }

  /**
   * Build strings from the operators() for 'select' options.
   */
  public function operatorOptions($which = 'title') {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  /**
   * Add this filter to the query.
   *
   * Due to the nature of fapi, the value and the operator have an unintended
   * level of indirection. You will find them in $this->operator
   * and $this->value respectively.
   */
  public function query() {
    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";
    $operator = $this->operator;
    $exp = "$field $operator inet(:filterValue)";

    $this->query->addWhereExpression($this->options['group'], $exp, [
      ':filterValue' => $this->value,
    ]);
  }

}
