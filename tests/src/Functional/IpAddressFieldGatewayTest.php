<?php

namespace Drupal\Tests\field_ipaddress_pgsql\Functional;

use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the IP address field when the "Add default gateway" option is enabled.
 *
 * @group field_ipaddress_pgsql
 */
class IpAddressFieldGatewayTest extends IpAddressFieldTestBase {

  /**
   * Tests the IP Address field with different settings and values.
   *
   * @param array $fieldStorageSettings
   *   The ip field instance settings.
   * @param array $value
   *   The associative array of the "IP address" and "Default Gateway" values.
   * @param string $expected
   *   The expected output for the field.
   * @param string $error
   *   The expected error message.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ElementTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @dataProvider ipAddressFieldDataProvider
   */
  public function testIpAddressField(array $fieldStorageSettings, array $value, string $expected = '', string $error = '') {
    // Apply field settings.
    $fieldStorage = FieldStorageConfig::loadByName('node', $this->fieldName);
    $fieldStorage->set('settings', $fieldStorageSettings)->save();

    // Create new node of test type.
    $this->drupalGet('node/add/' . $this->nodeType->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm([
      'title[0][value]' => 'Test node',
      $this->fieldName . '[0][value]' => $value['ip_address'],
      $this->fieldName . '[0][default_gateway]' => $value['default_gateway'],
    ], 'Save');

    if ($error) {
      $this->assertSession()->statusMessageContains($error, 'error');
    }
    elseif ($expected) {
      $this->assertSession()->addressMatches('/^\/node\/\d$/');
      $this->assertSession()->elementTextContains('css', 'article', $expected);
    }
  }

  /**
   * Data provider for testIpAddressField.
   */
  public function ipAddressFieldDataProvider() {
    return [
      // ---------- Data for "Positive Testing" -----------.
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '192.168.100.10/24',
          'default_gateway' => '192.168.100.1',
        ],
        '192.168.100.10/24 192.168.100.1',
      ],
      [
        [
          'family' => 4,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '192.168.100.0/24',
          'default_gateway' => '192.168.100.1',
        ],
        '192.168.100.0/24 192.168.100.1',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '::ffff:1.2.3.0/128',
          'default_gateway' => '::ffff:1.2.3.0',
        ],
        '::ffff:1.2.3.0/128 ::ffff:1.2.3.0',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '::ffff:1.2.3.0/120',
          'default_gateway' => '::ffff:1.2.3.0',
        ],
        '::ffff:1.2.3.0/120 ::ffff:1.2.3.0',
      ],
      // ---------- Data for "Negative Testing" -----------.
      // ---------- Invalid Default Gateway ---------------.
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '192.168.100.10/24',
          'default_gateway' => '192.168.100.0',
        ],
        '',
        'Invalid default gateway',
      ],
      [
        [
          'family' => 4,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '192.168.100.0/24',
          'default_gateway' => '192.168.100.0',
        ],
        '',
        'Invalid default gateway',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '::ffff:1.2.3.0/120',
          'default_gateway' => '::ffff:1.2.3',
        ],
        '',
        'Invalid default gateway',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '::ffff:1.2.3.0/120',
          'default_gateway' => '::ffff:1.2.3',
        ],
        '',
        'Invalid default gateway',
      ],
      // ---------- Invalid IP address --------------------.
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => 'qwerty',
          'default_gateway' => '192.168.100.1',
        ],
        '',
        'Invalid IP address format',
      ],
      // Invalid mask length.
      [
        [
          'family' => 4,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '192.168.100.10/33',
          'default_gateway' => '192.168.100.1',
        ],
        '',
        'Invalid IP address format',
      ],
      [
        [
          'family' => 4,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '192.168.100.10/24',
          'default_gateway' => '192.168.100.1',
        ],
        '',
        'The IP address value must follow',
      ],
      // The "::" sign can only appear once in a single IP address.
      [
        [
          'family' => 6,
          'cidr_format' => FALSE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '2001:db8:a0b:12f0::::0:1',
          'default_gateway' => '::ffff:1.2.3',
        ],
        '',
        'Invalid IP address format',
      ],
      [
        [
          'family' => 6,
          'cidr_format' => TRUE,
          'default_gateway_enabled' => TRUE,
        ],
        [
          'ip_address' => '2001:db8:1234::1/48',
          'default_gateway' => '::ffff:1.2.3',
        ],
        '',
        'The IP address value must follow',
      ],
    ];
  }

}
