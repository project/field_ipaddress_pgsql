<?php

namespace Drupal\field_ipaddress_pgsql\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;

/**
 * Defines a field mapper for "ipaddress_pgsql" Field.
 *
 * @FeedsTarget(
 *   id = "ipaddress_pgsql",
 *   field_types = {"ipaddress_pgsql"}
 * )
 */
class IpAddressPostgreSql extends FieldTargetBase {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    $definition = FieldTargetDefinition::createFromFieldDefinition($field_definition);
    $definition->addProperty('value');

    if ($field_definition->getSetting('default_gateway_enabled')) {
      $definition->addProperty('default_gateway');
    }
    return $definition;
  }

}
