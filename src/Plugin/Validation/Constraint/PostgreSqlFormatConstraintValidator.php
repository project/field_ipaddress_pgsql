<?php

declare(strict_types=1);

namespace Drupal\field_ipaddress_pgsql\Plugin\Validation\Constraint;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use IPTools\Network;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the PostgreSQL format validation constraint.
 */
final class PostgreSqlFormatConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Constructs the object.
   */
  public function __construct(
    private readonly Connection $connection,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $field, Constraint $constraint): void {
    $value = $field->value;

    try {
      $this->connection->query('SELECT inet(:ipAddressString)', [':ipAddressString' => $value]);
    }
    catch (\Exception $e) {
      $this->context->addViolation($constraint->messageIpAddressFormat);
      return;
    }

    $select = "SELECT family(inet :ipAddressString)";
    $query = $this->connection->query($select, [':ipAddressString' => $value]);
    $family = $query->fetchField();

    /** @var \Drupal\Core\Field\FieldItemListInterface $field */
    $settings = $field->getFieldDefinition()->getSettings();

    if ($family != $settings['family']) {
      if ($settings['family'] == 4) {
        $this->context->addViolation($constraint->messageIpV4);
      }
      else {
        $this->context->addViolation($constraint->messageIpV6);
      }
      return;
    }

    if ($settings['cidr_format']) {
      try {
        $this->connection->query('SELECT cidr(:ipAddressString)', [':ipAddressString' => $value]);
      }
      catch (\Exception $e) {
        $this->context->addViolation($constraint->messageCidr);
        return;
      }
    }

    // Validate default gateway if it was enabled in the field settings.
    if (!$settings['default_gateway_enabled']) {
      return;
    }

    // Validate the Default gateway if it was enabled in the field settings.
    // The "Default Gateway" is an IP Address that is within the subnet of the
    // "Network Interface". If the value of the "Network Interface" is
    // 192.168.100.10/24, then the valid value of "Default Gateway" should be
    // between 192.168.100.1 and 192.168.100.254.
    if (!$settings['default_gateway_enabled']) {
      return;
    }

    /** @var \IPTools\Range $hosts */
    $hosts = Network::parse($value)->hosts;

    try {
      /** @var \IPTools\IP $ipAddress */
      $defaultGateway = Network::parse($field->default_gateway)->getIP();
    }
    catch (\Exception $e) {
      $this->context->addViolation($constraint->defaultGateway, [
        '@firstIP' => (string) $hosts->getFirstIP(),
        '@lastIP' => (string) $hosts->getLastIP(),
      ]);
      return;
    }

    if (!$hosts->contains($defaultGateway)) {
      $this->context->addViolation($constraint->defaultGateway, [
        '@firstIP' => (string) $hosts->getFirstIP(),
        '@lastIP' => (string) $hosts->getLastIP(),
      ]);
    }

  }

}
